<?php

	// Mail settings
	$to = "alexmois@live.com";
	$subject = "Цифрус - обратная связь";

	if (isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["message"])) {

		$content  = "Имя: "     . $_POST["name"]    . "\r\n";
		$content .= "E-mail: "    . $_POST["email"]   . "\r\n";
		$content .= "Сообщение: "  . "\r\n" . $_POST["message"];

		if (mail($to, $subject, $content, $_POST["email"])) {

			$result = array(
				"message" => "Спасибо за обращение =)",
				"sendstatus" => 1
			);

			echo json_encode($result);

		} else {

			$result = array(
				"message" => "Ошибка. =(",
				"sendstatus" => 0
			);

			echo json_encode($result);

		}

	}

?>